var prefetch = (function(){
	'use strict';
	
	var prefetch = function(id, loaded, callback){
		this.position = 0;
		this.queue = [];
		this.percent = 0;
		this.status = id ? document.getElementById(id) : document.createElement('div');
		if (loaded) this.loaded = loaded
		if (callback) this.callback = callback
		this.init();
	};
	
	prefetch.prototype.init = function(){		
		var match;
		for (var i=0;i<document.styleSheets.length;i++) {
			var c = document.styleSheets[i].rules || document.styleSheets[i].cssRules;
			for (var j=0;j<=c.length-1;j++) {
				var string = c[j].cssText;
				if (typeof string == "undefined") {
					string = c[j].style.backgroundImage;
				}
				var pattern = /url\(([a-zA-z0-9:.\/\-\"\']+)\)/gi;
				while (match = pattern.exec(string)) {
					var path = match[0].replace('url(','').replace(')','').replace(/\"/g, '');
					this.addItem(path, document.styleSheets[i].href);
				}
			}
		}
	};
	
	prefetch.prototype.addItem = function(path, stylesheet){
		if (typeof path != "undefined") {
			if (path.substr(0,3) == "../") {
				path = path.substr(3);
				path = stylesheet.substr(0, stylesheet.substr(0, stylesheet.lastIndexOf('/')).lastIndexOf('/'))+"/"+path;
			}
			this.queue.push(path);
		}
	};
	
	prefetch.prototype.confirm = function() {
		this.process();
		this.position++;
	};
	
	prefetch.prototype.loader = function(path) {
		var img = new Image();
		img.onload = this.confirm.bind(this);
		img.onerror = this.confirm.bind(this);
		img.src = path;
	};
	
	prefetch.prototype.process = function() {
		this.progress();
		if (typeof this.queue[this.position] != "undefined") {
			this.loader(this.queue[this.position]);
			if (typeof this.loaded == "function") {
				this.loaded();
			}
		} else {
			if (typeof this.callback == "function") {
				this.callback();
			}
		}
	};
	
	prefetch.prototype.progress = function() {
		this.percent = Math.round((this.position / (this.queue.length)) * 100);
		if (this.status) {
			this.status.innerHTML = 'loading '+this.percent+'%';
		}
		return this.percent;
	};
	
	return prefetch;

}());
