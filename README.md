prefetch.js
============

Scans currently embedded stylesheets for paths and loads them before they actually become visible within the browser.

Loads all paths it finds. Doesn't quite work with @import yet or image paths that have more than one set of ../ or ./.

Great for use on large, ajax heavy websites or other web applications where you might require the user to have all assets loaded prior to engaging with content.

how do you use it?
============
Drop prefetch.js at the end of your body tag. Then load prefetch like this:

```
var demo = new prefetch('status', function(){
  console.log("Currently at this queue position: "+this.queue[this.position]);
}, function () {
  console.log("We've loaded everything.");
}).process();
```

callback when done?
============
There are two callbacks. One for 'loaded' and one for 'callback'. Loaded is to be executed after each load, good for updating progress bars and firing other events. Callback is for when we're 100% done.

browser support?
============
IE9+ and other modern browsers. If you want a bit of backwards compatibility, change the way the ```confirm``` method is called when the image object is created.

demo
============
http://zi.vc/prefetch/

to do list
============
- Add support for @import
- Add recursive ../
- Add parallel download support
- Create seperate queue objects for other domains
- Dynamically add items to the queue and load them automatically, even when we're done
